// Users
db.users.insertMany([
    {
        "firstName": "Stephen",
        "lastName": "Hawking",
        "age": 76,
        "email": "stephenhawking@mail.com",
        "department": "HR"
    },

    {
        "firstName": "Neil",
        "lastName": "Armstrong",
        "age": 82,
        "email": "neilarmstrong@mail.com",
        "department": "HR"
    },

    {
        "firstName": "Bill",
        "lastName": "Gates",
        "age": 65,
        "email": "billgates@mail.com",
        "department": "Operations"
    },

    {
        "firstName": "Jane",
        "lastName": "Doe",
        "age": 21,
        "email": "janedoe@mail.com",
        "department": "HR"
    }
])

// Finding users with letter s in their name or d in their last name
// Show only the firsName and lastName fields and hide the _id field

db.users.find({
    $or: [
    {"firstName": {
        $regex: 'S'
    }},
    {"lastName": {
        $regex: 'D'
    }}
    ]
    },
    {
        "_id": 0,
        "firstName": 1,
        "lastName": 1
    }
);

//alt
db.users.find({
    $or: [
    {"firstName": {
        $regex: 'S',
        $options: '$i'
    }},
    {"lastName": {
        $regex: 'D',
        $options: '$i'
    }}
    ]
    },
    {
        "_id": 0,
        "firstName": 1,
        "lastName": 1
    }
);


// Find users who are from the HR department and their age is greater than or equal to 70

db.users.find({
    $and: [
        {
            "department": "HR"
        },
        {
            "age": {$gte: 70}
        }
    ]
    }
    )

// Find users with the letter e in their first name and has an age or less than or equal to 30

db.users.find({
    $and:[
        {
            "firstName": {
                $regex: 'e'
            }
        },
        {
            "age": {
                $lte: 30
            }
        }
    ]
})

// alt
db.users.find({
    $and:[
        {
            "firstName": {
                $regex: 'E',
                $options: '$i'
            }
        },
        {
            "age": {
                $lte: 30
            }
        }
    ]
})